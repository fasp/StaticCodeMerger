import shutil, os, fileinput, time #, pdb
SRC = 'example_src'
DST = 'dst';
SRC_INCL = SRC+'/include';
DST_INCL = DST+'/include'; 

if os.path.exists(DST):
	shutil.rmtree(DST)

shutil.copytree(SRC, DST);
if os.path.exists(DST_INCL):
	shutil.rmtree(DST_INCL)
time.sleep(1)

#pdb.set_trace()
token = '#REPLACEWITH';
ext = '.html'
htmlFiles = [os.path.join(dp, f) for dp, dn, filenames in os.walk(DST) for f in filenames if os.path.splitext(f)[1] == ext]
for html in htmlFiles:
	filename = os.path.abspath(html);
	print 'processing '+filename+'...'
	for line in fileinput.input(filename, inplace=True):
		spaces = len(line)-len(line.lstrip());
		line = line.strip();
		comment = ((line[4:len(line)-4]).strip());
		if line.startswith('<!--') and line.rstrip().endswith('-->') and comment.startswith(token):
			path = SRC_INCL+'/'+(comment[len(token):]).strip();
			with open(path, 'r') as f:
				lineArr=f.read().splitlines();
				for repLine in lineArr:
					print repLine
			if path[path.rfind('/')+1:path.find(".html")] == 'navbar':
				name = filename[filename.rfind(os.sep)+1:filename.find(ext)];
				print "<script type=\"text/javascript\">"
				print "document.getElementById('navbar-"+name+"').setAttribute('class', 'active');"
				print "</script>"
		else:
			print(' '*spaces + line)
print '...done!'
os.system("pause")